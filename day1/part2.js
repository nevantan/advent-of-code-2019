const fs = require('fs');

const calculateFuel = (input, total = 0) => {
  const required = Math.floor(input / 3) - 2;

  if (required <= 0) return total;

  return calculateFuel(required, total + required);
};

(async () => {
  let input = await new Promise((resolve, reject) => {
    fs.readFile(__dirname + '/input.txt', 'utf8', (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

  let answer = input.split('\n').reduce((total, mass) => {
    return total + calculateFuel(parseInt(mass));
  }, 0);

  console.log(answer);
})();
