import fs from 'fs';

const execute = (address, memory) => {
  switch (memory[address]) {
    case 1: {
      // Addition
      const first = memory[memory[address + 1]];
      const second = memory[memory[address + 2]];
      memory[memory[address + 3]] = first + second;
      address += 4;
      break;
    }
    case 2: {
      // Multiplication
      const first = memory[memory[address + 1]];
      const second = memory[memory[address + 2]];
      memory[memory[address + 3]] = first * second;
      address += 4;
      break;
    }
    case 99:
      address = -1;
      break;
    default:
      throw new Error('Unrecognized opcode');
  }

  return [address, memory];
};

const run = (program, noun, verb) => {
  let address = 0;
  let memory = [...program];
  memory[1] = noun;
  memory[2] = verb;

  while (true) {
    const [next, state] = execute(address, memory);
    address = next;
    memory = state;

    if (address === -1) break;
  }

  return memory;
};

(async () => {
  const input = await new Promise((resolve, reject) => {
    fs.readFile(__dirname + '/input.txt', (err, data) => {
      if (err) return reject(err);
      return resolve(data.toString());
    });
  });

  let program = input.split(',').map((n) => parseInt(n));
  let result = 0;
  const target = 19690720;

  for (let noun = 0; noun <= 99; noun++) {
    for (let verb = 0; verb <= 99; verb++) {
      result = run(program, noun, verb)[0];

      if (result === target) {
        console.log(100 * noun + verb);
        return;
      }
    }
  }
})();
