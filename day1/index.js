const fs = require('fs');

(async () => {
  const input = await new Promise((resolve, reject) => {
    fs.readFile(__dirname + '/input.txt', 'utf8', (err, data) => {
      if (err) return reject(err);
      return resolve(data);
    });
  });

  let answer = input.split('\n').reduce((total, mass) => {
    return total + (Math.floor(mass / 3) - 2);
  }, 0);

  console.log(answer);
})();
